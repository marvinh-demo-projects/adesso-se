const hostURL = ".";

class HeaderRow extends React.Component {
    render() {
        const row = [];
        this.props.headerData.forEach(function (item){
            const field =  React.createElement('th', null, item);
            row.push(field);
        });
        return React.createElement('thead', null, React.createElement('tr', null, row));
    }
}

class DataRow extends React.Component {
    render() {
        const row = [];
        this.props.rowData.forEach(function (item){
           const field = React.createElement('td', null, `${item}`);
           row.push(field);
        });
        return React.createElement('tr', {onClick: () => {this.props.callback(this.props.rowData);}}, row);
    }
}

class DataTable extends React.Component {
    render() {
        const table = [];
        const headerRow = React.createElement(HeaderRow, {headerData: this.props.tableHeader}, null);
        table.push(headerRow);
        const callback = this.props.rowCallback;
        this.props.tableData.forEach(function (item){
            const row = React.createElement(DataRow, {
                rowData: item,
                callback: callback
            }, null);
            table.push(row);
        });
        return React.createElement('table', null, table);
    }
}

function queryContacts(containerID, onClickRow){
    const query = new XMLHttpRequest();
    query.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            const contacts = JSON.parse(this.responseText);

            const header = ['first name', 'last name', 'phone number'];

            const dataRows = [];
            contacts.forEach(function (contact){
                const row = [];
                row.push(contact.firstName);
                row.push(contact.lastName);
                row.push(contact.phoneNumber);
                dataRows.push(row);
            })

            ReactDOM.render(React.createElement(DataTable, {
                tableData: dataRows,
                tableHeader: header,
                rowCallback: onClickRow
            }, null), document.getElementById(containerID));
        }
    };
    query.open("GET", "./contacts", true);
    query.send();
}

function addContact(containerID, onClickRow, contactData){
    const query = new XMLHttpRequest();
    query.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            queryContacts(containerID, onClickRow);
        }
    };
    query.open("POST", "./contact", true);
    query.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    const requestBody = JSON.stringify(contactData);
    query.send(requestBody);
}

function deleteContact(containerID, onClickRow, firstName, lastName){
    const query = new XMLHttpRequest();
    query.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            queryContacts(containerID, onClickRow);
        }
    };
    query.open("DELETE", "./contact/"+lastName+"/"+firstName, true);
    query.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    query.send();
}

function updateContact(containerID, onClickRow, firstName, lastName, contactData){
    const query = new XMLHttpRequest();
    query.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            queryContacts(containerID, onClickRow);
        }
    };
    query.open("PUT", "./contact/"+lastName+"/"+firstName, true);
    query.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    const requestBody = JSON.stringify(contactData);
    query.send(requestBody);
}