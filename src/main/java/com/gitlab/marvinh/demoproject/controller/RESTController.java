package com.gitlab.marvinh.demoproject.controller;

import com.gitlab.marvinh.demoproject.repository.ContactDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@RestController
public class RESTController {
    private final ContactDataRepository contactDataRepository;

    @Autowired
    public RESTController(ContactDataRepository contactDataRepository) {
        this.contactDataRepository = contactDataRepository;
    }

    @GetMapping("/contacts")
    public Flux<ContactData> getContacts(){
        Stream<ContactData> contactDataStream = StreamSupport.stream(contactDataRepository.findAll().spliterator(), false).map(contactDataEntry -> {
            ContactData contactData = new ContactData();
            contactData.setFirstName(contactDataEntry.getFirstName());
            contactData.setLastName(contactDataEntry.getLastName());
            contactData.setPhoneNumber(contactDataEntry.getPhoneNumber());
            return contactData;
        });
        return Flux.fromStream(contactDataStream);
    }

    @PostMapping("/contact")
    public void createContact(
            @RequestBody ContactData contactData
    ){
        ContactDataRepository.ContactDataEntry entry = new ContactDataRepository.ContactDataEntry();
        entry.setFirstName(contactData.getFirstName()+" - Marvin");
        entry.setLastName(contactData.getLastName()+" - Haagen");
        entry.setPhoneNumber(contactData.getPhoneNumber());
        contactDataRepository.save(entry);
    }

    @PutMapping("/contact/{lastName}/{firstName}")
    public void updateContact(
            @PathVariable String lastName,
            @PathVariable String firstName,
            @RequestBody ContactData contactData
    ){
        ContactDataRepository.ContactDataEntry entry = contactDataRepository.findFirstByFirstNameAndLastName(firstName, lastName);
        entry.setFirstName(contactData.getFirstName());
        if (!entry.getFirstName().endsWith(" - Marvin")){
            entry.setFirstName(entry.getFirstName()+" - Marvin");
        }
        entry.setLastName(contactData.getLastName());
        if (!entry.getLastName().endsWith(" - Haagen")){
            entry.setLastName(entry.getLastName()+" - Haagen");
        }
        entry.setPhoneNumber(contactData.getPhoneNumber());
        contactDataRepository.save(entry);
    }

    @DeleteMapping("/contact/{lastName}/{firstName}")
    public void deleteContact(
            @PathVariable String lastName,
            @PathVariable String firstName
    ){
        contactDataRepository.deleteAllByFirstNameAndLastName(firstName, lastName);
    }
}
