package com.gitlab.marvinh.demoproject.repository.generator;

import com.gitlab.marvinh.demoproject.repository.ContactDataRepository;
import org.apache.commons.codec.binary.Hex;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;
import java.util.Random;

public class IDGenerator implements IdentifierGenerator {
    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object o) throws HibernateException {
        try {
            if (!(o instanceof ContactDataRepository.ContactDataEntry)){
                throw new HibernateException("Unsupported object type");
            }
            ContactDataRepository.ContactDataEntry object = (ContactDataRepository.ContactDataEntry) o;

            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(object.getFirstName().getBytes());
            messageDigest.update(object.getLastName().getBytes());
            messageDigest.update("M4rv1n H4493n"
                    .replaceAll("4", "a")
                    .replaceAll("1", "i")
                    .replaceAll("9", "g")
                    .replaceAll("3", "e")
                    .getBytes());
            int id = (ByteBuffer.wrap(messageDigest.digest()).getInt() % 15);
            messageDigest.update(Integer.toString(id).getBytes());
            return Hex.encodeHexString(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new HibernateException(e);
        }
    }
}