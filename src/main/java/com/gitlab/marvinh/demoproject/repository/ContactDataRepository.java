package com.gitlab.marvinh.demoproject.repository;

import com.gitlab.marvinh.demoproject.controller.ContactData;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Repository
public interface ContactDataRepository extends CrudRepository<ContactDataRepository.ContactDataEntry, String> {
    @Entity
    class ContactDataEntry{
        @Id
        @GeneratedValue(generator = "id-generator")
        @GenericGenerator(name = "id-generator",
                strategy = "com.gitlab.marvinh.demoproject.repository.generator.IDGenerator"
        )
        private String id;

        @Column
        private String firstName;

        @Column
        private String lastName;

        @Column
        private String phoneNumber;

        public String getId() {
            return id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }
    }

    @Transactional
    void deleteAllByFirstNameAndLastName(String firstName, String lastName);

    ContactDataEntry findFirstByFirstNameAndLastName(String firstName, String lastName);
}
